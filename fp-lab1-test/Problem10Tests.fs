namespace Test

open NUnit.Framework

[<TestFixture>]
type Problem10Tests() =

    static member Problem10Cases() =
        [ 10, 17L
          100, 1060L
          1_000, 76127L
          10_000, 5736396L
          100_000, 454396537L
          1_000_000, 37550402023L
          2_000_000, 142913828922L
          849_165, 27_442_241_530L
          365_794, 5_442_726_964L
          1_920_921, 132_231_254_181L
          1_595_215, 92_362_582_769L
          1_939_870, 134_769_742_212L
          1_637_522, 97_182_751_809L
          1_529_202, 85_201_585_127L
          1_200_007, 53_434_606_138L
          1_196_701, 53_147_000_014L ]
        |> List.map (fun (n, res) -> TestCaseData(n, res))


    [<TestCaseSource("Problem10Cases")>]
    member this.Recursive(n, res) =
        Assert.AreEqual(res, Task10.recursive.compute n)

    [<TestCaseSource("Problem10Cases")>]
    member this.TailRecursive(n, res) =
        Assert.AreEqual(res, Task10.tail_recursive.compute n)

    [<TestCaseSource("Problem10Cases")>]
    member this.Cyclic(n, res) =
        Assert.AreEqual(res, Task10.cyclic.compute n)

    [<TestCaseSource("Problem10Cases")>]
    member this.ModularNormal(n, res) =
        Assert.AreEqual(res, Task10.modular.normal.compute n)

    [<TestCaseSource("Problem10Cases")>]
    member this.Map(n, res) =
        Assert.AreEqual(res, Task10.map.compute n)

    [<TestCaseSource("Problem10Cases")>]
    member this.Seq(n, res) =
        Assert.AreEqual(res, Task10.seq.compute n)
