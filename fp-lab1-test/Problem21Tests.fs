namespace Test

open NUnit.Framework

[<TestFixture>]
type Problem21Tests() =

    static member Problem21Cases() =
        [ 1, 0
          10, 0
          100, 0
          1_000, 504
          10_000, 31_626
          1_038, 504
          2_861, 5_518
          7_698, 31_626
          8_734, 31_626
          6_813, 31_626
          4_929, 8_442
          2_394, 2_898
          2_270, 2_898
          7_924, 31_626 ]
        |> List.map (fun (n, res) -> TestCaseData(n, res))

    [<Test>]
    [<TestCaseSource("Problem21Cases")>]
    member this.Recursive(n, res) =
        Assert.AreEqual(res, Task21.recursive.compute n)

    [<Test>]
    [<TestCaseSource("Problem21Cases")>]
    member this.TailRecursive(n, res) =
        Assert.AreEqual(res, Task21.tail_recursive.compute n)

    [<Test>]
    [<TestCaseSource("Problem21Cases")>]
    member this.Monolith(n, res) =
        Assert.AreEqual(res, Task21.monolith.compute n)

    [<TestCaseSource("Problem21Cases")>]
    member this.Cyclic(n, res) =
        Assert.AreEqual(res, Task21.cyclic.compute n)

    [<TestCaseSource("Problem21Cases")>]
    member this.Map(n, res) =
        Assert.AreEqual(res, Task21.map.compute n)

    [<TestCaseSource("Problem21Cases")>]
    member this.Seq(n, res) =
        Assert.AreEqual(res, Task21.seq.compute n)
