public class MainClass
{
    public static IEnumerable<int> GetDivisors(int n)
        => Enumerable.Range(1, n / 2  + 1).Where(i => n % i == 0);

    public static bool IsAmicableNumber(int a)
    {
        var b = GetDivisors(a).Sum();
        var db = GetDivisors(b).Sum();
        
        return a == db && a != b;
    }
    
    public static int SumOfAmicableNumbersLessThan(int n)
    {
        var sum = 0;
        for (var i = 1; i < n; i++)
            if (IsAmicableNumber(i))
                sum += i;
        
        return sum;
    }
    
    public static void Main(string[] args)
    {
        var n = 10_000;
        Console.WriteLine(SumOfAmicableNumbersLessThan(n));
    }
}