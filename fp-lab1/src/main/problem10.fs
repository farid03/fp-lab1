﻿module Task10

module common =
    let isPrime (n: int64) =
        let root = float n |> System.Math.Sqrt |> int64 |> (+) 1L

        let rec isPrimeCheck i =
            if i >= root then true
            elif n % i = 0L then false
            else isPrimeCheck (i + 1L)

        if n = 1L then false else isPrimeCheck 2L

module recursive =
    let compute n =
        let isPrime (n: int64) =
            let rec isPrimeCheck (n: int64) (i: int64) =
                if i <= 1L then true
                elif (n % i) = 0L then false
                else isPrimeCheck n (i - 1L)

            let root = float n |> System.Math.Sqrt |> int64
            isPrimeCheck n root

        [ 2L .. n ] |> List.filter isPrime |> List.sum

module tail_recursive =
    let compute n =
        let isPrime (n: int64) =
            let root = float n |> System.Math.Sqrt |> int64 |> (+) 1L

            let rec isPrimeCheck i =
                if i >= root then true
                elif n % i = 0L then false
                else isPrimeCheck (i + 1L)

            if n = 1L then false else isPrimeCheck 2L

        [ 2L .. n ] |> List.filter isPrime |> List.sum

module cyclic =
    let isPrime n = common.isPrime n

    let compute n =
        let mutable sum = 0L

        for i in 2L .. n do
            if isPrime i then
                sum <- sum + i

        sum

module modular =
    module completely_recursive =
        let compute n =
            let isPrime n = common.isPrime n

            let rec generateSequence (a: int64) =
                seq {
                    yield a
                    yield! generateSequence (a + 1L)
                }

            let rec filterSeq filter sequence =
                seq {
                    match Seq.tryHead sequence with
                    | None -> ()
                    | Some head when filter head ->
                        yield head
                        yield! filterSeq filter (Seq.skip 1 sequence)
                    | Some _ -> yield! filterSeq filter (Seq.skip 1 sequence)
                }

            let sum sequence =
                let rec sum sequence (acc: int64) =
                    match Seq.tryHead sequence with
                    | None -> acc
                    | Some head -> sum (Seq.skip 1 sequence) (acc + head)

                sum sequence 0

            generateSequence 2
            |> Seq.takeWhile (fun x -> x <= n)
            |> filterSeq isPrime
            |> sum

    module normal =
        let compute n =
            let isPrime n = common.isPrime n
            let getNumbers n = [ 2L .. n ]
            let filter list = List.filter isPrime list
            let sum list = List.sum list

            getNumbers n |> filter |> sum

module map =
    let compute n =
        let isPrime n = common.isPrime n

        let sum list = List.fold (+) 0L list

        let map = List.map (fun x -> if isPrime x then x else 0L)

        [ 2L .. n ] |> map |> sum

module seq =
    let compute n =
        let isPrime n = common.isPrime n

        let sequenceOfPrimes =
            let rec generateSequence (a: int64) =
                seq {
                    if isPrime a then
                        yield a

                    yield! generateSequence (a + 1L)
                }

            generateSequence 2L

        sequenceOfPrimes |> Seq.takeWhile (fun x -> x <= n) |> Seq.sum
