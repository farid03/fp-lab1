module Main

let task10Example () =
    printfn "> Euler task 10: Summation of primes"
    let n = 2_000_000L

    Task10.recursive.compute n |> printfn "%A | 1.1 recursive"
    Task10.tail_recursive.compute n |> printfn "%A | 1.2 tail recursive"
    Task10.modular.normal.compute n |> printfn "%A | 2 modular"
    Task10.map.compute n |> printfn "%A | 3 with map"
    Task10.cyclic.compute n |> printfn "%A | 4 with cycle"
    Task10.seq.compute n |> printfn "%A | 5  with sequence"

    Task10.modular.completely_recursive.compute 200 |> ignore // extra

let rec task21Example () =
    printfn "> Euler task 21: Amicable numbers"
    let n = 10000

    Task21.recursive.compute n |> printfn "%A | 1.1 recursive modular"
    Task21.tail_recursive.compute n |> printfn "%A | 1.2 tail recursive modular"
    Task21.monolith.compute n |> printfn "%A | 1.3 monolith"

    Task21.map.compute n |> printfn "%A | 3 with map"
    Task21.cyclic.compute n |> printfn "%A | 4 with cycle"
    Task21.seq.compute n |> printfn "%A | 5  with sequence"

[<EntryPoint>]
let main args =
    task10Example ()
    task21Example ()

    0
