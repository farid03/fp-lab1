public class MainClass
{
    public static bool IsPrime(int n)
        => n == 2 || Enumerable.Range(2, (int) Math.Sqrt(n)).All(i => n % i != 0);

    public static IEnumerable<int> GetPrimesLessThan(int n) =>
        Enumerable.Range(2, n - 1).Where(IsPrime);
    
    public static long SumOfPrimesLessThan(int n) =>
        GetPrimesLessThan(n).Aggregate(0L, (a, b) => a + b);
    
    public static void Main(string[] args)
    {
        var n = 2_000_000;
        Console.WriteLine(SumOfPrimesLessThan(n));
    }
}