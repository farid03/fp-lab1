module public Task21

module common =
    let getDivisors x =
        [ 1 .. x / 2 + 1 ] |> List.filter (fun y -> x % y = 0)

module recursive =
    let compute n =
        let getDivisors n =
            let rec divisors cur n =
                if cur <= 0 then []
                elif n % cur = 0 then cur :: divisors (cur - 1) n
                else divisors (cur - 1) n

            divisors (n - 1) n

        let getDivisorsSum n = getDivisors n |> List.sum

        let isAmicable a =
            let b = getDivisorsSum a
            let db = getDivisorsSum b

            a = db && a <> b

        let rec sumAmicable n acc =
            if n = 0 then acc
            elif isAmicable n then sumAmicable (n - 1) (acc + n)
            else sumAmicable (n - 1) acc

        sumAmicable n 0



module tail_recursive =
    let compute n =
        let getDivisors n =

            let cond = n / 2 + 1

            let rec divisors cur n =
                if cur >= cond then []
                elif n % cur = 0 then cur :: divisors (cur + 1) n
                else divisors (cur + 1) n

            divisors 1 n

        let getDivisorsSum n =
            let divisors = getDivisors n

            let rec sum i acc =
                if i = divisors.Length then
                    acc
                else
                    sum (i + 1) (acc + divisors[i])

            sum 0 0

        let isAmicable a =
            let b = getDivisorsSum a
            let db = getDivisorsSum b

            a = db && a <> b

        let rec sumAmicable n acc =
            if n = 0 then acc
            elif isAmicable n then sumAmicable (n - 1) (acc + n)
            else sumAmicable (n - 1) acc

        sumAmicable n 0


module monolith =
    let compute n =
        [ 1..n ]
        |> List.filter (fun x ->
            let acc = ([ 1 .. x / 2 + 1 ] |> List.filter (fun y -> x % y = 0) |> List.sum)

            x = ([ 1 .. acc / 2 + 1 ] |> List.filter (fun y -> acc % y = 0) |> List.sum)
            && x <> ([ 1 .. x / 2 + 1 ] |> List.filter (fun y -> x % y = 0) |> List.sum))
        |> List.sum

module map =
    let compute n =
        let getDivisors n = common.getDivisors n

        let getDivisorsSum n = getDivisors n |> List.sum

        let map x =
            if x = getDivisorsSum (getDivisorsSum x) && x <> getDivisorsSum x then
                x
            else
                0

        List.sumBy map [ 1..n ]

module cyclic =
    let compute n =
        let getDivisors n = common.getDivisors n

        let getDivisorsSum n =
            let mutable sum = 0

            for i in getDivisors n do
                sum <- sum + i

            sum

        let mutable sum = 0

        for i in [ 1..n ] do
            let a = getDivisorsSum i
            let b = getDivisorsSum a

            if i = b && i <> a then
                sum <- sum + i

        sum

module seq =
    let compute n =
        let getDivisors n =
            Seq.initInfinite (fun x -> x + 1)
            |> Seq.takeWhile (fun x -> x <= n - 1)
            |> Seq.filter (fun x -> n % x = 0)

        let getDivisorsSum n = getDivisors n |> Seq.sum

        let getAmicableNumbers =
            seq {
                for i in Seq.initInfinite (fun x -> x + 1) do
                    let a = getDivisorsSum i
                    let b = getDivisorsSum a

                    if i = b && i <> a then
                        yield i
            }

        getAmicableNumbers |> Seq.takeWhile (fun x -> x <= n) |> Seq.sum
